changelog of the Scilab Imsls Toolbox

Imsls (0.1)
 * Import from Scilin.
 * Updated to Scilab 5 toolbox standard.
 * Removed unrelated functions.
 * Removed rotmat (used only in gmres).
 * Renamed scripts (e.g. from bicg to imsls_bicg).
 * Refactored Scilab's pcg with imsls_pcg.
 * Update of the macros to generate the help from the sources.
 * Added missing support functions.
 * Updated comments of support functions.
 * Added help automatic updater.
 * Updated comments to generate the help.
 * Added examples.
 * Added examples for test matrices.
 * Fixed startup script.
 * Added getpath
 * Updated demos.
 * Renamed beta into bet to avoid name conflict.
 * Added test matrices.
 * Created imsls_benchmatrix for benchmarks.
 * Added optional graphics to benchmatrix.
 * Created copyright headers.
 * Fixed all demos.
 * Updated benchmarks.
 * Fixed comments in the header of bicg for the parameters description.
 * Updated formating of the source code for standard coding.
 * Added unit test for bicg.
 * Added unit test for tester function.
 * Added unit test for benchmatrix.
 * Added a few words of description for each algorithm.
 * Added overview.
 * Added test for mtlb_pcg.
 * Added ref for unit test of pcg.
 * Added internal functions.
 * Added a second preconditionner M2 to pcg.
 * Modified interface of callbacks of imsls_qmr to match Matlab.
 * Created compatibility mtlb_qmr function, with help and unit test.
 * Updated the examples for the matvec and precond functions.
 * Updated implementation of imsls_bicg.
 * Updated implementation of imsls_bicgstab.
 * Updated implementation of imsls_cgs.
 * Added tests with extra-args for matvec.
 * Updated implementation of imsls_cheby.
 * Updated implementation of imsls_gmres.
 * Updated implementation of imsls_jacobi.
 * Updated implementation of imsls_sor.
 * Clarified algorithms which cannot provide A as a function.
 * Checked which solvers require to have A symetric.
   Added unit tests to make sure of this.
   Removed the check when no necessary.
 * Added extra-arguments to callbacks (matrix-vector
   product and preconditionners).
   Checked this with unit tests.
 * Added unit tests for bicgstab, cgs, cheby, jacobi, sor.
 * Fixed most examples in the help pages.
 * Fixed implementation of default M, example and
   unit test of imsls_cheby.
 * Added unit tests for lehmer, matgen, nonsym and split.
 * Added unit tests for wathen.
 * Added preconditionner M2 to gmres.
 * Added preconditionner M2 to bicg.
 * Added preconditionner M2 to bicgstab.
 * Added preconditionner M2 to cgs.
 * Created mtlb_gmres for compatibility.
 * Added Quick Start section in Overview.
 * Added message on Overview at startup.
 * Clarified content of err output variable in most functions.
 * Created drafts of imsls_bicg.
 * Fixed bug in preconditionning M2 of bicg.
 * Finalized mtlb_bicg.
 * Created mtlb_bicgstab for compatibility.
 * Added a test to check that all serious solvers
   accept A as sparse or full.
 * Created spdiags function for compatibility.
 * Created mtlb_cgs for compatibility.
 * Created "Compatibility" help section.
 * Added the comment that any optional input argument equal to the
   empty matrix [] is replaced by its default value.
 * Fixed all mtlb examples and tests.
 * Added a warning when the flag is nonzero, that is, when the
   output x does not satisfy the required tolerance.
 * Fixed convergence of examples and tests for all solvers
   after warning for no convergence.
 * Added a section on non convergence in the overview.
 * Fixed the names of the demos: demo1, demo2, demo3
   were not very explicit.
   Added pde225 and nos3.
 * Fixed input argument checking in matrix generators.
 * Used apifun_checkflint where appropriate.
 * Fixed the internal lib loading system, to
   make it work under ATOMS.
