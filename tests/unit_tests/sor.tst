// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

A=imsls_makefish(4);
xe = (1:16)';
b=A*xe;
x0=zeros(16,1);
[x,err,iter,flag,res] = imsls_sor(A,b,x0);
assert_checkequal(size(res,"*"),iter+1);
assert_checkalmostequal(x,xe,1.e-0);
assert_checkequal(iter,16);
assert_checkequal(flag,1);
assert_checktrue(err<1.e-1);
assert_checkequal(size(res,"*"),iter+1);
assert_checktrue(res<=1);
assert_checktrue(res($)<1.e-1);
//
A=imsls_makefish(4);
xe = (1:16)';
b=A*xe;
x0=zeros(16,1);
max_it=200;
tol=1000*%eps;
w=0.8;
[x,err,iter,flag,res] = imsls_sor(A,b,[],w,max_it,tol);
assert_checkequal(size(res,"*"),iter+1);
assert_checkalmostequal(x,xe,1.e-5);
assert_checkequal(iter,106);
assert_checkequal(flag,0);
assert_checktrue(err<1.e-6);
assert_checkequal(size(res,"*"),iter+1);
assert_checktrue(res<=1);
assert_checktrue(res($)<1.e-6);
//
// Check the warning when no convergence
max_it=50;
tol=1000*%eps;
w=0.8;
[x,err,iter,flag,res] = imsls_sor(A,b,x0,w,max_it,tol);
assert_checkequal(flag,1);

