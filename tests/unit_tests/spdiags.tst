// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2009 - DIGITEO - Yann Collette
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

// Test 1 (m<n) - full

A = [
     0 5 0 10  0  0
     0 0 6  0 11  0
     3 0 0  7  0 12
     1 4 0  0  8  0
     0 2 5  0  0  9
];
[B, d] = imsls_spdiags(A);
B_ref = [
         0 0 5 10
         0 0 6 11
         0 3 7 12
         1 4 8  0
         2 5 9  0
];
d_ref = [-3 -2 1 3]';
assert_checkequal(B,B_ref);
assert_checkequal(d,d_ref);

// Test 1 (m>n) - full
A = [
    0    0    3    1    0
    5    0    0    4    2
    0    6    0    0    5
   10    0    7    0    0
    0   11    0    8    0
    0    0   12    0    9
];
[B, d] = imsls_spdiags(A);
B_ref = [
   10    5    0    0
   11    6    0    0
   12    7    3    0
    0    8    4    1
    0    9    5    2
];
d_ref = [-3 -1 2 3]';
assert_checkequal(B,B_ref);
assert_checkequal(d,d_ref);

// Test 1 (m<n) - sparse

A = [
     0 5 0 10  0  0
     0 0 6  0 11  0
     3 0 0  7  0 12
     1 4 0  0  8  0
     0 2 5  0  0  9
];
A = sparse(A);
[B, d] = imsls_spdiags(A);
B_ref = [
         0 0 5 10
         0 0 6 11
         0 3 7 12
         1 4 8  0
         2 5 9  0
];
d_ref = [-3 -2 1 3]';
assert_checkequal(B,B_ref);
assert_checkequal(d,d_ref);

// Test 1 (m>n) - sparse
A = [
    0    0    3    1    0
    5    0    0    4    2
    0    6    0    0    5
   10    0    7    0    0
    0   11    0    8    0
    0    0   12    0    9
];
A = sparse(A);
[B, d] = imsls_spdiags(A);
B_ref = [
   10    5    0    0
   11    6    0    0
   12    7    3    0
    0    8    4    1
    0    9    5    2
];
d_ref = [-3 -1 2 3]';
assert_checkequal(B,B_ref);
assert_checkequal(d,d_ref);

//
// Test 2 / part 1
//
n = 10;
e = ones(n,1);
A = imsls_spdiags([e -2*e e], -1:1, n, n);
assert_checkequal(typeof(A),"sparse");
A_ref = [
  -2   1   0   0   0   0   0   0   0   0
   1  -2   1   0   0   0   0   0   0   0
   0   1  -2   1   0   0   0   0   0   0
   0   0   1  -2   1   0   0   0   0   0
   0   0   0   1  -2   1   0   0   0   0
   0   0   0   0   1  -2   1   0   0   0
   0   0   0   0   0   1  -2   1   0   0
   0   0   0   0   0   0   1  -2   1   0
   0   0   0   0   0   0   0   1  -2   1
   0   0   0   0   0   0   0   0   1  -2
   ];
assert_checkequal(full(A),A_ref);

// Test 2 / part 2 - full
n = 10;
e = ones(n,1);
A = [
  -2   1   0   0   0   0   0   0   0   0
   1  -2   1   0   0   0   0   0   0   0
   0   1  -2   1   0   0   0   0   0   0
   0   0   1  -2   1   0   0   0   0   0
   0   0   0   1  -2   1   0   0   0   0
   0   0   0   0   1  -2   1   0   0   0
   0   0   0   0   0   1  -2   1   0   0
   0   0   0   0   0   0   1  -2   1   0
   0   0   0   0   0   0   0   1  -2   1
   0   0   0   0   0   0   0   0   1  -2
   ];
d = 0;
A2 = imsls_spdiags(abs(-(n-1)/2:(n-1)/2)',d,A);
A_ref = [
   4.5   1    0    0    0    0    0    0    0    0
   1   3.5    1    0    0    0    0    0    0    0
   0     1  2.5    1    0    0    0    0    0    0
   0     0    1  1.5    1    0    0    0    0    0
   0     0    0    1  0.5    1    0    0    0    0
   0     0    0    0    1  0.5    1    0    0    0
   0     0    0    0    0    1  1.5    1    0    0
   0     0    0    0    0    0    1  2.5    1    0
   0     0    0    0    0    0    0    1  3.5    1
   0     0    0    0    0    0    0    0    1  4.5
   ];
assert_checkequal(A2,A_ref);

// Test 2 / part 2 - sparse
n = 10;
e = ones(n,1);
A = [
  -2   1   0   0   0   0   0   0   0   0
   1  -2   1   0   0   0   0   0   0   0
   0   1  -2   1   0   0   0   0   0   0
   0   0   1  -2   1   0   0   0   0   0
   0   0   0   1  -2   1   0   0   0   0
   0   0   0   0   1  -2   1   0   0   0
   0   0   0   0   0   1  -2   1   0   0
   0   0   0   0   0   0   1  -2   1   0
   0   0   0   0   0   0   0   1  -2   1
   0   0   0   0   0   0   0   0   1  -2
   ];
A = sparse(A);
d = 0;
A2 = imsls_spdiags(abs(-(n-1)/2:(n-1)/2)',d,A);
assert_checkequal(typeof(A2),"sparse");
A_ref = [
   4.5   1    0    0    0    0    0    0    0    0
   1   3.5    1    0    0    0    0    0    0    0
   0     1  2.5    1    0    0    0    0    0    0
   0     0    1  1.5    1    0    0    0    0    0
   0     0    0    1  0.5    1    0    0    0    0
   0     0    0    0    1  0.5    1    0    0    0
   0     0    0    0    0    1  1.5    1    0    0
   0     0    0    0    0    0    1  2.5    1    0
   0     0    0    0    0    0    0    1  3.5    1
   0     0    0    0    0    0    0    0    1  4.5
   ];
assert_checkequal(full(A2),A_ref);

// Test 2 / part 3
A = [
   4.5   1    0    0    0    0    0    0    0    0
   1   3.5    1    0    0    0    0    0    0    0
   0     1  2.5    1    0    0    0    0    0    0
   0     0    1  1.5    1    0    0    0    0    0
   0     0    0    1  0.5    1    0    0    0    0
   0     0    0    0    1  0.5    1    0    0    0
   0     0    0    0    0    1  1.5    1    0    0
   0     0    0    0    0    0    1  2.5    1    0
   0     0    0    0    0    0    0    1  3.5    1
   0     0    0    0    0    0    0    0    1  4.5
   ];
B = imsls_spdiags(A);
B_ref = [
   1   4.5   0
   1   3.5   1
   1   2.5   1
   1   1.5   1
   1   0.5   1
   1   0.5   1
   1   1.5   1
   1   2.5   1
   1   3.5   1
   0   4.5   1
   ];
assert_checkequal(B,B_ref);


// Test 2

n = 10;
e = ones(n,1);
A = imsls_spdiags([e -2*e e], -1:1, n, n);
A_ref = [
  -2   1   0   0   0   0   0   0   0   0
   1  -2   1   0   0   0   0   0   0   0
   0   1  -2   1   0   0   0   0   0   0
   0   0   1  -2   1   0   0   0   0   0
   0   0   0   1  -2   1   0   0   0   0
   0   0   0   0   1  -2   1   0   0   0
   0   0   0   0   0   1  -2   1   0   0
   0   0   0   0   0   0   1  -2   1   0
   0   0   0   0   0   0   0   1  -2   1
   0   0   0   0   0   0   0   0   1  -2
   ];
assert_checkequal(full(A),A_ref);
A = imsls_spdiags(abs(-(n-1)/2:(n-1)/2)',0,A);
A_ref = [
   4.5   1    0    0    0    0    0    0    0    0
   1   3.5    1    0    0    0    0    0    0    0
   0     1  2.5    1    0    0    0    0    0    0
   0     0    1  1.5    1    0    0    0    0    0
   0     0    0    1  0.5    1    0    0    0    0
   0     0    0    0    1  0.5    1    0    0    0
   0     0    0    0    0    1  1.5    1    0    0
   0     0    0    0    0    0    1  2.5    1    0
   0     0    0    0    0    0    0    1  3.5    1
   0     0    0    0    0    0    0    0    1  4.5
   ];
assert_checkequal(full(A),A_ref);
B = imsls_spdiags(A);
B_ref = [
   1   4.5   0
   1   3.5   1
   1   2.5   1
   1   1.5   1
   1   0.5   1
   1   0.5   1
   1   1.5   1
   1   2.5   1
   1   3.5   1
   0   4.5   1
   ];
assert_checkequal(B,B_ref);

// Test 3

A = [11    0   13    0
      0   22    0   24
      0    0   33    0
     41    0    0   44
      0   52    0    0
      0    0   63    0
      0    0    0   74];

[B,d] = imsls_spdiags(A);

d_ref = [-3 0 2]';

B_ref = [41 11  0
         52 22  0
         63 33 13
         74 44 24];

assert_checkequal(B,B_ref);
assert_checkequal(d,d_ref);

// Test 4

B = [
    1  1  1  1  1  1  1
    2  2  2  2  2  2  2
    3  3  3  3  3  3  3
    4  4  4  4  4  4  4
    5  5  5  5  5  5  5
    6  6  6  6  6  6  6
];
d = [-4 -2 -1 0 3 4 5]';
A = imsls_spdiags(B,d,6,6);
A_ref = [
         1 0 0 4 5 6
         1 2 0 0 5 6
         1 2 3 0 0 6
         0 2 3 4 0 0
         1 0 3 4 5 0
         0 2 0 4 5 6
];
assert_checkequal(full(A),A_ref);

// Example 5A
B = [
   1    6   11
   2    7   12
   3    8   13
   4    9   14
   5   10   15
];
// Part 1 — m is equal to n.
A = imsls_spdiags(B, [-2 0 2], 5, 5);
A_ref = [
6    0   13    0    0
0    7    0   14    0
1    0    8    0   15
0    2    0    9    0
0    0    3    0   10
];
assert_checkequal(full(A),A_ref);
// Part 2 — m is greater than n.
A = imsls_spdiags(B, [-2 0 2], 5, 4);
A_ref = [
6    0   13    0
0    7    0   14
1    0    8    0
0    2    0    9
0    0    3    0
];
assert_checkequal(full(A),A_ref);
// Part 3 — m is less than n.
A = imsls_spdiags(B, [-2 0 2], 4, 5);
A_ref = [
6    0   11    0    0
0    7    0   12    0
3    0    8    0   13
0    4    0    9    0
];
assert_checkequal(full(A),A_ref);
//
//
// Example 5B
//
// Part 1.
A = [
6    0   13    0    0
0    7    0   14    0
1    0    8    0   15
0    2    0    9    0
0    0    3    0   10
];
B_ref = [
1   6   0
2   7   0
3   8  13
0   9  14
0  10  15
];
B = imsls_spdiags(A);
assert_checkequal(B,B_ref);
//
// Part 2.
//
A = [
6    0   13    0
0    7    0   14
1    0    8    0
0    2    0    9
0    0    3    0
];
B_ref = [
1   6   0
2   7   0
3   8  13
0   9  14
];
B = imsls_spdiags(A);
assert_checkequal(B,B_ref);
//
// Part 3.
//
A = [
6    0   11    0    0
0    7    0   12    0
3    0    8    0   13
0    4    0    9    0
];
B_ref = [
0   6  11
0   7  12
3   8  13
4   9   0
];
B = imsls_spdiags(A);
assert_checkequal(B,B_ref);
//
// Tests from http://www.cmi.univ-mrs.fr/~herbin/ANANUM/MATLAB/tp.html
//
A=imsls_spdiags([ 1; 2; 3], [ 0 ], 3, 3);
assert_checkequal(typeof(A),"sparse");
A_ref = [
     1     0     0
     0     2     0
     0     0     3
];
assert_checkequal(full(A),A_ref);
//
A=imsls_spdiags([ 1; 2; 3], [ 0 ], 4, 3);
assert_checkequal(typeof(A),"sparse");
A_ref = [
     1     0     0
     0     2     0
     0     0     3
     0     0     0
];
assert_checkequal(full(A),A_ref);
//
d1=(11:14)';
d2=(21:24)';
A=imsls_spdiags([d1 d2], [-1 1], 4, 4);
assert_checkequal(typeof(A),"sparse");
A_ref = [
     0    22     0     0
    11     0    23     0
     0    12     0    24
     0     0    13     0
];
assert_checkequal(full(A),A_ref);
//
n=5;
e=ones(n,1);
A=imsls_spdiags([e 2*e e], [-1 0 1], n, n);
assert_checkequal(typeof(A),"sparse");
A_ref = [
     2     1     0     0     0
     1     2     1     0     0
     0     1     2     1     0
     0     0     1     2     1
     0     0     0     1     2
];
assert_checkequal(full(A),A_ref);
//
n=8;
A=imsls_spdiags([[1:n]' [ones(n,1)*[1:n-1]]], [0:n-1], n+1, n);
assert_checkequal(typeof(A),"sparse");
A_ref = [
     1     1     2     3     4     5     6     7
     0     2     1     2     3     4     5     6
     0     0     3     1     2     3     4     5
     0     0     0     4     1     2     3     4
     0     0     0     0     5     1     2     3
     0     0     0     0     0     6     1     2
     0     0     0     0     0     0     7     1
     0     0     0     0     0     0     0     8
     0     0     0     0     0     0     0     0
];
assert_checkequal(full(A),A_ref);

