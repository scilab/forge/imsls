// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

//
// Try some solvers with full matrix
//
A=imsls_makefish(4);
xe = (1:16)';
b=A*xe;
M = A;
imsls_benchmatrix(A,b,[],M);
//
// Try some solvers with sparse matrix
//
A=imsls_makefish(4);
A=sparse(A);
xe = (1:16)';
b=A*xe;
M = A;
imsls_benchmatrix(A,b,[],M);

