// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

n = 100;
on = ones(n,1);
A = imsls_spdiags([-2*on 4*on -on],-1:1,n,n)
b = sum(A,"c");
tol = 1e-8;
maxit = 15;
M1 = imsls_spdiags([on/(-2) on],-1:0,n,n);
M2 = imsls_spdiags([4*on -on],0:1,n,n);
[x, relres, iter, flag, resvec] = mtlb_bicg(A,b,tol,maxit,M1,M2);
xe = ones(n,1);
assert_checkalmostequal(x,xe);
assert_checktrue(relres<1.e-8);
assert_checkequal(iter,9);
assert_checkequal(size(resvec),[10 1]);
assert_checktrue(resvec($)<1.e-8);

//
//
//
n = 100;
tol = 1e-8;
maxit = 15;
on = ones(n,1);
M1 = imsls_spdiags([on/(-2) on],-1:0,n,n);
M2 = imsls_spdiags([4*on -on],0:1,n,n);

    function y = afun(x,transp_flag)
       if (transp_flag=="transp") then
          // y = A'*x
          y = 4 * x;
          y(1:n-1) = y(1:n-1) - 2 * x(2:n);
          y(2:n) = y(2:n) - x(1:n-1);
       elseif (transp_flag=="notransp") then
          // y = A*x
          y = 4 * x;
          y(2:n) = y(2:n) - 2 * x(1:n-1);
          y(1:n-1) = y(1:n-1) - x(2:n);
       end
    endfunction
on = ones(n,1);
b = afun(on,"notransp");
[x, relres, iter, flag, resvec] = mtlb_bicg(afun,b,tol,maxit,M1,M2);
xe = ones(n,1);
assert_checkalmostequal(x,xe);
assert_checktrue(relres<1.e-8);
assert_checkequal(iter,9);
assert_checkequal(size(resvec),[10 1]);
assert_checktrue(resvec($)<1.e-8);

