// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

//
// All parameters by default.
// No convergence.
A=imsls_lehmer(16);
xe = (1:16)';
b=A*xe;
[x,err,iter,flag,res] = imsls_qmr(A,b);
assert_checkalmostequal(x,xe,1.e-5);
assert_checkequal(iter,16);
assert_checkequal(flag,1);
assert_checktrue(err<1.e-7);
assert_checkequal(size(res,"*"),iter+1);
assert_checktrue(res<=1);
assert_checktrue(res($)<1.e-7);
//
// With x0
// No convergence.
x0=zeros(16,1);
[x,err,iter,flag,res] = imsls_qmr(A,b,x0);
assert_checkalmostequal(x,xe,1.e-5);
assert_checkequal(iter,16);
assert_checkequal(flag,1);
assert_checktrue(err<1.e-7);
assert_checktrue(res<=1);
assert_checktrue(res($)<1.e-7);
//
// With a matrix preconditionner M
// No convergence.
M=eye(16,16);
max_it=16;
tol=1000*%eps;
[x,err,iter,flag,res] = imsls_qmr(A,b,x0,M,[],max_it,tol);
assert_checkalmostequal(x,xe,1.e-5);
assert_checkequal(iter,16);
assert_checkequal(flag,1);
assert_checktrue(err<1.e-7);
assert_checktrue(res<=1);
assert_checktrue(res($)<1.e-7);
//
// With a function preconditionner M
// No convergence.
function y=precond(x,transp)
    if ( transp=="notransp" ) then
        y=4*eye(16,16)\x
    elseif ( transp=="transp" ) then
        y=4*eye(16,16)'\x
    else
        error(msprintf("Unknown transp: %s",transp))
    end
endfunction
[x,err,iter,flag,res] = imsls_qmr(A,b,x0,precond);
assert_checkalmostequal(x,xe,1.e-5);
assert_checkequal(iter,16);
assert_checkequal(flag,1);
assert_checktrue(err<1.e-7);
assert_checktrue(res<=1);
assert_checktrue(res($)<1.e-7);
//
// With a function Matrix-Vector A
// No convergence.
function y=matvec(x,transp)
    if ( transp=="notransp" ) then
        y=imsls_lehmer(16)*x
    elseif ( transp=="transp" ) then
        y=imsls_lehmer(16)'*x
    else
        error(msprintf("Unknown transp: %s",transp))
    end
endfunction
[x,err,iter,flag,res] = imsls_qmr(matvec,b);
assert_checkalmostequal(x,xe,1.e-5);
assert_checkequal(iter,16);
assert_checkequal(flag,1);
assert_checktrue(err<1.e-7);
assert_checktrue(res<=1);
assert_checktrue(res($)<1.e-7);
//
// To convergence
A=imsls_lehmer(16);
xe = (1:16)';
b=A*xe;
[x,err,iter,flag,res] = imsls_qmr(A,b,[],[],[],30);
assert_checkalmostequal(x,xe,1.e-5);
assert_checkequal(iter,21);
assert_checkequal(flag,0);
assert_checktrue(err<1.e-7);
assert_checkequal(size(res,"*"),iter+1);
assert_checktrue(res<=1);
assert_checktrue(res($)<1.e-7);

//
// With a function Matrix-Vector A and extra-args
//
function y=matvec2(x,transp,A,B)
    if ( transp=="notransp" ) then
        y=(A+B)*x
    elseif ( transp=="transp" ) then
        y=(A+B)'*x
    else
        error(msprintf("Unknown transp: %s",transp))
    end
endfunction
A = imsls_lehmer(16)/2;
B = A;
[x,err,iter,flag,res] = imsls_qmr(list(matvec2,A,B),b,[],[],[],200);
assert_checkalmostequal(x,xe,1.e-5);
assert_checkequal(iter,21);
assert_checkequal(flag,0);
assert_checktrue(err<1.e-7);
assert_checktrue(res<=1);
assert_checktrue(res($)<1.e-7);

//
// Test with a nonsymetric matrix
//
n=110 ;
A=imsls_nonsym(-10,-1,n);
xe = (1:n)';
b=A*xe;
[x,err,iter,flag,res] = imsls_qmr(A,b,[],[],[],200);
assert_checkequal(size(res,"*"),iter+1);
assert_checkalmostequal(x,xe,1.e-4);
// Do not check iter (perhaps varies depending on OS)
assert_checktrue(iter>100);
assert_checkequal(flag,0);
assert_checktrue(err<1.e-5);
assert_checkequal(size(res,"*"),iter+1);
assert_checktrue(res<=1000);
assert_checktrue(res($)<1.e-5);

