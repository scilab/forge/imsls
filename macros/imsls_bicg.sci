// Copyright (C) 1993 - Richard Barrett, Michael Berry, Tony F. Chan, James Demmel, June M. Donato, Jack Dongarra, Victor Eijkhout, Roldan Pozo, Charles Romine, and Henk Van der Vorst
// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [x, err, iter, flag, res] = imsls_bicg(varargin)
    // Solves linear equations using BiConjugate Gradient Method with preconditioning.
    //
    // Calling Sequence
    //  x = imsls_bicg(A, b)
    //  x = imsls_bicg(A, b, x0)
    //  x = imsls_bicg(A, b, x0, M1)
    //  x = imsls_bicg(A, b, x0, M1, M2)
    //  x = imsls_bicg(A, b, x0, M1, M2, max_it)
    //  x = imsls_bicg(A, b, x0, M1, M2, max_it, tol)
    //  [x, err] = imsls_bicg(...)
    //  [x, err, iter] = imsls_bicg(...)
    //  [x, err, iter, flag] = imsls_bicg(...)
    //  [x, err, iter, flag, res] = imsls_bicg(...)
    //
    // Parameters
    //    A:        a n-ny-n full or sparse matrix of doubles, symmetric positive definite matrix,  or a function returning <literal>A*x</literal> or <literal>A'*x</literal> or a list.
    //    b:        a n-ny-1 full or sparse matrix of doubles, right hand side vector
    //    x0:       a n-ny-1 full or sparse matrix of doubles, initial guess vector (default: zeros(n,1))
    //    M1:        a n-ny-n full or sparse matrix of doubles, left preconditioner matrix (default eye(n,n)) or a function returning <literal>M1\x</literal> or <literal>M1'\x</literal>.
    //    M2:        a n-ny-n full or sparse matrix of doubles, right preconditioner matrix (default eye(n,n)) or a function returning <literal>M2\x</literal> or <literal>M2'\x</literal>.
    //    max_it:   a 1-ny-1 matrix of doubles, integer value,  maximum number of iterations (default n)
    //    tol:      a 1-ny-1 matrix of doubles, positive,  relative error tolerance on x (default 1000*%eps)
    //    x:        a n-ny-1 full or sparse matrix of doubles,  solution vector
    //    err:      a 1-ny-1 matrix of doubles, final relative residual norm. This is equal to norm(A*x-b)/norm(b) if norm(b) is nonzero, and is equal to norm(A*x-b) if norm(b) is zero.
    //    iter:     a 1-ny-1 matrix of doubles, integer value,  number of iterations performed
    //    flag:     a 1-ny-1 matrix of doubles, integer value, 0: solution found to tolerance, 1: no convergence given max_it, -1 = breakdown
    //    res:      a (iter+1)-ny-1 full or sparse matrix of doubles, history of residual. res(1) is the initial residual and res(i+1) is the residual for the iteration i, for i=1,2,...,iter.
    //
    // Description
    // Solves the linear system Ax=b using the
    // BiConjugate Gradient Method with preconditioning.
    //
    // Any optional input argument equal to the empty matrix [] is replaced by its default value.
    //
    // The argument A can be a function returning <literal>A*x</literal> or <literal>A'*x</literal>.
    // In this case, the function A must have the header :
    //   <programlisting>
    //     y = A ( x , transp )
    //   </programlisting>
    // where x is the current vector and transp is a 1-by-1 matrix of strings.
    // If transp="notransp", then the A function must return y=A*x.
    // If transp="transp", then the A function must return y=A'*x.
    //
    // It might happen that the function requires additionnal arguments to be evaluated.
    // In this case, we can use the following feature.
    // The argument A can also be the list (funA,a1,a2,...).
    // In this case funA, the first element in the list, must have the header:
    //   <programlisting>
    //     y = funA ( x , transp, a1 , a2 , ... )
    //   </programlisting>
    // where the input arguments a1, a2, ...
    // are automatically be appended at the end of the calling sequence.
    //
    // The argument M1 can be a function returning <literal>M1\x</literal> or <literal>M1'\x</literal>.
    // In this case, the function M1 must have the header :
    //   <programlisting>
    //     y = M1 ( x , transp)
    //   </programlisting>
    // where x in the current vector and transp is a 1-by-1 matrix of strings.
    // If transp="notransp", then the M1 function must return y=M1\x.
    // If transp="transp", then the M1 function must return y=M1'\x.
    //
    // The argument M1 can be the list (funM1,a1,a2,...).
    // In this case funM1, the first element in the list, must have the header:
    //   <programlisting>
    //     y = funM1 ( x , transp , a1 , a2 , ... )
    //   </programlisting>
    // where the input arguments a1, a2, ...
    // are automatically be appended at the end of the calling sequence.
    //
    // The same feature is available for M2.
    //
    // The Biconjugate Gradient method generates two CG-like sequences of vectors, one
    // based on a system with the original coefﬁcient matrix A, and one on AT
    // Instead of orthogonalizing each sequence, they are made mutually orthogonal, or "bi-orthogonal".
    // This method, like CG, uses limited storage.
    // It is useful when the matrix is nonsymmetric and nonsingular;
    // however, convergence may be irregular, and there is a possibility
    // that the method will break down.
    // BiCG requires a multiplication with the coefﬁcient
    // matrix and with its transpose at each iteration.
    //
    // Examples
    // A=imsls_makefish(4);
    // xe=(1:16)';
    // b=A*xe;
    // x0=zeros(16,1);
    // [x,err,iter,flag,res] = imsls_bicg(A,b,x0)
    //
    // M1=eye(16,16);
    // M2=eye(16,16);
    // max_it=16;
    // tol=1000*%eps;
    // [x,err,iter,flag,res] = imsls_bicg(A,b,x0,M1,M2,max_it,tol);
    //
    // function y=precondM1(x,transp)
    //   if ( transp == "notransp") then
    //     // y=M\x
    //     y = 4*eye(16,16)\x
    //   elseif ( transp == "transp") then
    //     // y=M'\x
    //     y = 4*eye(16,16)'\x
    //   else
    //     error(msprintf("Unknown transp: %s",transp))
    //   end
    // endfunction
    //
    // function y=matvec(x,transp)
    //   A = imsls_makefish(4);
    //   if ( transp == "notransp") then
    //     y = A*x
    //   elseif ( transp == "transp") then
    //     y = A'*x
    //   else
    //     error(msprintf("Unknown transp: %s",transp))
    //   end
    // endfunction
    //
    // [x,err,iter,flag,res] = imsls_bicg(matvec,b,x0,precondM1,[],,max_it,tol);
    //
    // [x,err,iter,flag,res] = imsls_bicg(matvec,b,x0,M1,[],max_it,tol);
    // [x,err,iter,flag,res] = imsls_bicg(A,b,x0,precondM1,max_it,tol);
    //
    //
    // Bibliography
    //     Univ. of Tennessee and Oak Ridge National Laboratory, October 1, 1993., Details of this algorithm are described in "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and van der Vorst, SIAM Publications, 1993. (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).
    //     "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and Van der Vorst, SIAM Publications, 1993 (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).
    //     "Iterative Methods for Sparse Linear Systems, Second Edition", Saad, SIAM Publications, 2003 (ftp ftp.cs.umn.edu; cd dept/users/saad/PS; get all_ps.zip).
    //     http://www.netlib.org/templates/matlab//bicg.m
    //
    // Authors
    // Copyright (C) 1993 - Richard Barrett, Michael Berry, Tony F. Chan, James Demmel, June M. Donato, Jack Dongarra, Victor Eijkhout, Roldan Pozo, Charles Romine, and Henk Van der Vorst
    // Copyright (C) 2005 - INRIA - Sage Group (IRISA)
    // Copyright (C) 2011 - DIGITEO - Michael Baudin

    // Load Internals lib if it's not already loaded
    // =========================================================================
    if ~ exists("imslsinternalslib") then
        path = imsls_getpath()
        imslsinternalslib = lib(fullfile(path,"macros","internals"));
    end

    [lhs, rhs] = argn()
    apifun_checkrhs ( "imsls_bicg" , rhs , 2:7 )
    apifun_checklhs ( "imsls_bicg" , lhs , 0:5 )

    //
    // Get input arguments
    A = varargin(1);
    b = varargin(2);
    n = size(b,"*")
    x0 = apifun_argindefault (varargin , 3 , zeros(n,1) );
    M1 = apifun_argindefault(varargin , 4 , [] );
    M2 = apifun_argindefault(varargin , 5 , [] );
    max_it = apifun_argindefault(varargin , 6 , n )
    tol = apifun_argindefault(varargin , 7 , 1000*%eps )
    //
    // Check types
    apifun_checktype ( "imsls_bicg" , A ,  "A" , 1 , ["constant" "sparse" "function" "list"])
    if ( typeof(A)=="list" ) then
        apifun_checktype ( "imsls_bicg" , A(1) ,  "A(1)" , 1 , "function" )
    end
    apifun_checktype ( "imsls_bicg" , b ,  "b" , 2 , ["constant" "sparse"])
    apifun_checktype ( "imsls_bicg" , x0 , "x0" , 3 , ["constant" "sparse"])
    apifun_checktype ( "imsls_bicg" , M1 ,  "M1" , 4 , ["constant" "sparse" "function" "list"])
    if ( typeof(M1)=="list" ) then
        apifun_checktype ( "imsls_bicg" , M1(1) ,  "M1(1)" , 4 , "function" )
    end
    apifun_checktype ( "imsls_bicg" , M2 ,  "M2" , 5 , ["constant" "sparse" "function" "list"])
    if ( typeof(M2)=="list" ) then
        apifun_checktype ( "imsls_bicg" , M2(1) ,  "M2(1)" , 5 , "function" )
    end
    apifun_checktype ( "imsls_bicg" , max_it , "max_it" , 6 , "constant")
    apifun_checktype ( "imsls_bicg" , tol ,    "tol" , 7 , "constant")
    //
    // Check size
    if ( or ( typeof(A) == ["constant" "sparse"]) ) then
        apifun_checksquare ( "imsls_bicg" , A , "A" , 1 )
    end
    apifun_checkveccol ( "imsls_bicg" , b , "b" , 2 , size(b,"*") )
    apifun_checkveccol ( "imsls_bicg" , x0 , "x0" , 3 , size(x0,"*") )
    if ( or ( typeof(M1) == ["constant" "sparse"]) ) then
        if ( M1<> [] ) then
            apifun_checkdims ( "imsls_bicg" , M1 , "M1" , 4 , [n n] )
        end
    end
    if ( or ( typeof(M2) == ["constant" "sparse"]) ) then
        if ( M2<> [] ) then
            apifun_checkdims ( "imsls_bicg" , M2 , "M2" , 5 , [n n] )
        end
    end
    apifun_checkscalar ( "imsls_bicg" , max_it , "max_it" , 6 )
	apifun_checkflint  ( "imsls_bicg" , max_it , "max_it" , 6)
    apifun_checkscalar ( "imsls_bicg" , tol , "tol" , 7 )
    //
    // Check content
    apifun_checkgreq ( "imsls_bicg" , max_it , "max_it" , 6 , 1 )
	apifun_checkflint( "imsls_bicg" , max_it , "max_it" , 6)
    apifun_checkgreq ( "imsls_bicg" , tol , "tol" , 7 , number_properties("tiny") )
    //
    // Setup the Matrix-Vector product A into a couple (function,list-of-args).
    [myA_fun,myA_args] = imsls_setupMatvec(A,imsls_matvectrans);
    //
    // Setup the Left Preconditionner M1
    [myM1_fun,myM1_args] = imsls_setupPrecond(M1,imsls_precondtransfake,imsls_precondtrans);
    //
    // Setup the Right Preconditionner M2
    [myM2_fun,myM2_args] = imsls_setupPrecond(M2,imsls_precondtransfake,imsls_precondtrans);


    // initialization
    i = 0;
	iter = 0;
    flag = 0;
    x = x0;

    bnrm2 = norm( b );
    if  ( bnrm2 == 0.0 ) then
        bnrm2 = 1.0;
    end

    //  r = b - A*x;

    r = b -  myA_fun(x,"notransp",myA_args(1:$));
    err = norm( r ) / bnrm2;
    res = err;
    if ( err < tol ) then
        return;
    end

    r_tld  = r;

    for i = 1:max_it                    // begin iteration

        //     z = M2\(M1 \ r);
        z = myM1_fun(r,"notransp",myM1_args(1:$));
        z = myM2_fun(z,"notransp",myM2_args(1:$));

        //     z_tld = M1'\(M2' \ r_tld);
        // This is because (M1*M2)'=(M2')*(M1')
        z_tld = myM2_fun(r_tld,"transp",myM2_args(1:$));
        z_tld = myM1_fun(z_tld,"transp",myM1_args(1:$));

        rho   = ( z'*r_tld );
        if ( rho == 0.0 ) then
            break
        end

        if ( i > 1 ) then                    // direction vectors
            bet = rho / rho_1;
            p   = z  + bet*p;
            p_tld = z_tld + bet*p_tld;
        else
            p = z;
            p_tld = z_tld;
        end

        //     q = A*p;
        q = myA_fun(p,"notransp",myA_args(1:$)); // compute residual pair

        //     q_tld = A'*p_tld;
        q_tld = myA_fun(p_tld,"transp",myA_args(1:$));

        alppha = rho / (p_tld'*q );

        x = x + alppha*p;                    // update approximation
        r = r - alppha*q;
        r_tld = r_tld - alppha*q_tld;

        err = norm( r ) / bnrm2;          // check convergence
        res= [res;err];

        if ( err <= tol ) then
            iter=i;
            break;
        end

        rho_1 = rho;

        if ( i == max_it ) then
            iter=i;
        end

    end

    if ( err <= tol ) then                   // converged
        flag =  0;
    elseif ( rho == 0.0 ) then                 // breakdown
        flag = -1;
    else
        flag = 1;                           // no convergence
    end

    if ( flag <> 0 ) then
        warning(msprintf(gettext("%s: Algorithm has not converged."),"imsls_bicg"))
    end
endfunction

