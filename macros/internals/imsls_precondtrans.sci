// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function y = imsls_precondtrans(x,transp,M)
	// A true preconditionner
  if ( transp == "notransp") then
    y=M\x
  elseif ( transp == "transp") then
    y=M'\x
    else
        error(msprintf(gettext("%s: Unknown transp: %s","imsls_precondtrans"),transp))
  end
endfunction
