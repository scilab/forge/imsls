// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [myM_fun,myM_args] = imsls_setupPrecond(M,defaultMfake,defaultM)
    // Setup the left preconditionner M into a couple (function,list-of-args).
    //
	// M: a full or sparse matrix of doubles
	//    or a function performing M\x, with header y=f(x,M)
	//    or a list, where the first elements is f and the
	//    remaining elements are the extra-arguments of f.
	//    In this last case, the header is y=f(x,a1,...)
	//
	
    if ( or ( typeof(M) == ["constant" "sparse"]) ) then
        if ( M==[] ) then
            myM_fun = defaultMfake;
            myM_args = list();
        else
            myM_fun = defaultM;
            myM_args = list(M);
        end
    elseif ( typeof(M) == "function" ) then
        myM_fun = M;
        myM_args = list();
    else
        myM_fun = M(1);
        myM_args = list(M(2:$));
    end
endfunction
