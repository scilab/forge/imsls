// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function A = imsls_nonsym(a,b,n)
    // Returns a non symetric matrix.
    //
    // Calling Sequence
    //  A = imsls_nonsym(a,b,n)
    //
    // Parameters
    //    a: a 1-by-1 matrix of doubles, the first parameter
    //    b: a 1-by-1 matrix of doubles, the second parameter
    //    n: a 1-by-1 matrix of doubles, integer value, the size of the output matrix
    //    A: a n-by-n matrix of doubles
    //
    // Description
    // Returns a nonsymmetric matrix.
    // The matrix is a band matrix, with half band width equal to 1.
    //
    // Examples
    // n=110 ;
    // A=imsls_nonsym(-10,-1,n);
    //
    // Bibliography
    //     TODO
    //
    // Authors
    // Copyright (C) 2005 - INRIA - Sage Group (IRISA)
    // Copyright (C) 2011 - DIGITEO - Michael Baudin

	[lhs, rhs] = argn()
    apifun_checkrhs ( "imsls_nonsym" , rhs , 3 )
    apifun_checklhs ( "imsls_nonsym" , lhs , 1 )
    //
    // Check types
    apifun_checktype ( "imsls_nonsym" , a ,  "a" , 1 , "constant")
    apifun_checktype ( "imsls_nonsym" , b ,  "b" , 2 , "constant")
    apifun_checktype ( "imsls_nonsym" , n ,  "n" , 3 , "constant")
    //
    // Check size
    apifun_checkscalar ( "imsls_nonsym" , a ,  "a" , 1 )
    apifun_checkscalar ( "imsls_nonsym" , b ,  "b" , 2 )
    apifun_checkscalar ( "imsls_nonsym" , n ,  "n" , 3 )
    //
    // Check content
    apifun_checkgreq ( "imsls_wathen" , n ,  "n" , 3 , 1 )
	apifun_checkflint( "imsls_wathen" , n ,  "n" , 3)

	//
	
    h=1/(n+1);
    y=(2+a*h+b*h^2) ;
    x=-(1+a*h);
    z=-1;

    x = x*ones(n-1,1);
    z = z*ones(n-1,1);
    y = y*ones(n,1);

    A = diag(x, -1) + diag(y) + diag(z, 1);


endfunction
