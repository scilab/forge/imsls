// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [x, err, iter, flag, res] = imsls_qmr( varargin)
    // Solves linear equations using Quasi Minimal Residual Method with preconditioning.
    //
    // Calling Sequence
    //  x = imsls_qmr ( A, b)
    //  x = imsls_qmr ( A, b, x0)
    //  x = imsls_qmr ( A, b, x0, M1)
    //  x = imsls_qmr ( A, b, x0, M1, M2)
    //  x = imsls_qmr ( A, b, x0, M1, M2, max_it)
    //  x = imsls_qmr ( A, b, x0, M1, M2, max_it, tol )
    //  [x, err] = imsls_qmr(...)
    //  [x, err, iter] = imsls_qmr(...)
    //  [x, err, iter, flag] = imsls_qmr(...)
    //  [x, err, iter, flag, res] = imsls_qmr(...)
    //
    // Parameters
    //    A:        a n-ny-n full or sparse nonsymmetric matrix of doubles or a function returning <literal>A*x</literal> or <literal>A'*x</literal> or a list.
    //    b:        a n-ny-1 full or sparse matrix of doubles, right hand side vector
    //    x0:       a n-ny-1 full or sparse matrix of doubles, initial guess vector (default: zeros(n,1))
    //    M1:       a n-ny-n full or sparse matrix of doubles, the left preconditioner matrix (default eye(n,n)) or a function returning <literal>M1\x</literal> or <literal>M1'\x</literal>.
    //    M2:       a n-ny-n full or sparse matrix of doubles, the right preconditioner matrix (default eye(n,n)) or function returning <literal>M2\x</literal> or <literal>M2'\x</literal>
    //    max_it:   a 1-ny-1 matrix of doubles, integer value,  maximum number of iterations (default n)
    //    tol:      a 1-ny-1 matrix of doubles, positive,  relative error tolerance on x (default 1000*%eps)
    //    x:        a n-ny-1 full or sparse matrix of doubles,  solution vector
    //    err:      a 1-ny-1 matrix of doubles, final relative residual norm. This is equal to norm(A*x-b)/norm(b) if norm(b) is nonzero, and is equal to norm(A*x-b) if norm(b) is zero.
    //    iter:     a 1-ny-1 matrix of doubles, integer value,  number of iterations performed
    //    flag:     a 1-ny-1 matrix of doubles, integer value, 0: solution found to tolerance, 1: no convergence given max_it. Negative values indicate breakdown: -1: rho, -2: bet, -3: gam, -4: delta, -5: ep, -6: xi
    //    res:      a (iter+1)-ny-1 full or sparse matrix of doubles, history of residual. res(1) is the initial residual and res(i+1) is the residual for the iteration i, for i=1,2,...,iter.
    //
    // Description
    // Solves the linear system Ax=b using the
    // Quasi Minimal Residual Method with preconditioning.
    //
    // Any optional input argument equal to the empty matrix [] is replaced by its default value.
    //
    // The argument A can be a function returning <literal>A*x</literal> or <literal>A'*x</literal>.
    // In this case, the function A must have the header :
    //   <programlisting>
    //     y = A ( x , transp )
    //   </programlisting>
    // where x is the current vector and transp is a 1-by-1 matrix of strings.
    // If transp="notransp", then the A function must return y=A*x.
    // If transp="transp", then the A function must return y=A'*x.
    //
    // It might happen that the function requires additionnal arguments to be evaluated.
    // In this case, we can use the following feature.
    // The argument A can also be the list (funA,a1,a2,...).
    // In this case funA, the first element in the list, must have the header:
    //   <programlisting>
    //     y = funA ( x , transp, a1 , a2 , ... )
    //   </programlisting>
    // where the input arguments a1, a2, ...
    // are automatically be appended at the end of the calling sequence.
    //
    // The argument M1 can be a function returning <literal>M1\x</literal> or <literal>M1'\x</literal>.
    // In this case, the function M1 must have the header :
    //   <programlisting>
    //     y = M1 ( x , transp)
    //   </programlisting>
    // where x in the current vector and transp is a 1-by-1 matrix of strings.
    // If transp="notransp", then the M1 function must return y=M1\x.
    // If transp="transp", then the M1 function must return y=M1'\x.
    //
    // The argument M1 can be the list (funM1,a1,a2,...).
    // In this case funM1, the first element in the list, must have the header:
    //   <programlisting>
    //     y = funM1 ( x , transp , a1 , a2 , ... )
    //   </programlisting>
    // where the input arguments a1, a2, ...
    // are automatically be appended at the end of the calling sequence.
    //
    // The same feature is available for M2.
    //
    // The Quasi-Minimal Residual method applies a least-squares solve and update to the
    // BiCG residuals, thereby smoothing out the irregular convergence behavior of BiCG.
    // Also, QMR largely avoids the breakdown that can occur in BiCG.
    // On the other hand, it does not effect a true minimization of either the error or the residual, and while it
    // converges smoothly, it does not essentially improve on the BiCG.
    //
    // Examples
    // A=imsls_makefish(4);
    // xe=(1:16)';
    // b=A*xe;
    // [x,err,iter,flag,res] = imsls_qmr(A,b)
    // // Plot Iterations vs 2-norm Residual
    // scf();
    // plot(1:iter+1,log10(res),"b*-");
    // xtitle("Convergence of QMR","Iterations","Logarithm of 2-norm of residual")
    //
	// // With initial guess
    // x0=zeros(16,1);
    // [x,err,iter,flag,res] = imsls_qmr(A,b,x0);
    //
	// // With preconditionners
    // M1=eye(16,16);
    // M2=M1;
    // max_it=16;
    // tol=1000*%eps;
    // [x,err,iter,flag,res] = imsls_qmr(A,b,x0,M1,M2,max_it,tol);
    //
    // // Configure callbacks
    // function y=matvec(x,transp)
    //   A=imsls_makefish(4);
    //   if ( transp == "notransp") then
    //     y = A*x
    //   elseif ( transp == "transp") then
    //     y = A'*x
    //   else
    //     error(msprintf("Unknown transp: %s",transp))
    //   end
    // endfunction
    //
    // function y=precondM1(x,transp)
    //   if ( transp == "notransp") then
    //     // y=M1\x
    //     y = 4*eye(16,16)\x
    //   elseif ( transp == "transp") then
    //     // y=M1'\x
    //     y = 4*eye(16,16)'\x
    //   else
    //     error(msprintf("Unknown transp: %s",transp))
    //   end
    // endfunction
    //
    // function y=precondM2(x,transp)
    //   if ( transp == "notransp") then
    //     // y=M2\x
    //     y = eye(16,16)\x
    //   elseif ( transp == "transp") then
    //     // y=M2'\x
    //     y = eye(16,16)'\x
    //   else
    //     error(msprintf("Unknown transp: %s",transp))
    //   end
    // endfunction
    //
    // [x,err,iter,flag,res] = imsls_qmr(matvec,b,x0,precondM1);
    // [x,err,iter,flag,res] = imsls_qmr(matvec,b,x0,precondM1,precondM2);
    //
    // // See with extra arguments.
    // function y=matvec(x,transp,A)
    //   if ( transp == "notransp") then
    //     y = A*x
    //   elseif ( transp == "transp") then
    //     y = A'*x
    //   else
    //     error(msprintf("Unknown transp: %s",transp))
    //   end
    // endfunction
    // A=imsls_makefish(4);
    // xe=(1:16)';
    // b=A*xe;
    // [x,err,iter,flag,res] = imsls_qmr(list(matvec,A),b);
    //
    // Bibliography
    //     Univ. of Tennessee and Oak Ridge National Laboratory, October 1, 1993., Details of this algorithm are described in "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and van der Vorst, SIAM Publications, 1993. (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).
    //     "Templates for the Solution of Linear Systems: Building Blocks for Iterative Methods", Barrett, Berry, Chan, Demmel, Donato, Dongarra, Eijkhout, Pozo, Romine, and Van der Vorst, SIAM Publications, 1993 (ftp netlib2.cs.utk.edu; cd linalg; get templates.ps).
    //     "Iterative Methods for Sparse Linear Systems, Second Edition", Saad, SIAM Publications, 2003 (ftp ftp.cs.umn.edu; cd dept/users/saad/PS; get all_ps.zip).
    //     http://www.netlib.org/templates/matlab//qmr.m
    //
    // Authors
    // Copyright (C) 2011 - DIGITEO - Michael Baudin

    // Load Internals lib if it's not already loaded
    // =========================================================================
    if ~ exists("imslsinternalslib") then
        path = imsls_getpath()
        imslsinternalslib = lib(fullfile(path,"macros","internals"));
    end

    [lhs, rhs] = argn()
    apifun_checkrhs ( "imsls_qmr" , rhs , 2:7 )
    apifun_checklhs ( "imsls_qmr" , lhs , 0:5 )

    //
    // Get input arguments
    A = varargin(1);
    b = varargin(2);
    n = size(b,"*")
    x0 = apifun_argindefault(varargin , 3 , zeros(n,1) );
    M1 = apifun_argindefault(varargin , 4 , [] );
    M2 = apifun_argindefault(varargin , 5 , [] );
    max_it = apifun_argindefault(varargin , 6 , n )
    tol = apifun_argindefault(varargin , 7 , 1000*%eps )
    //
    // Check types
    apifun_checktype ( "imsls_qmr" , A ,  "A" , 1 , ["constant" "sparse" "function" "list"])
    if ( typeof(A)=="list" ) then
        apifun_checktype ( "imsls_qmr" , A(1) ,  "A(1)" , 1 , "function" )
    end
    apifun_checktype ( "imsls_qmr" , b ,  "b" , 2 , ["constant" "sparse"])
    apifun_checktype ( "imsls_qmr" , x0 , "x0" , 3 , ["constant" "sparse"])
    apifun_checktype ( "imsls_qmr" , M1 ,  "M1" , 4 , ["constant" "sparse" "function" "list"])
    if ( typeof(M1)=="list" ) then
        apifun_checktype ( "imsls_qmr" , M1(1) ,  "M1(1)" , 4 , "function" )
    end
    apifun_checktype ( "imsls_qmr" , M2 , "M2" , 5 , ["constant" "sparse" "function" "list"])
    if ( typeof(M2)=="list" ) then
        apifun_checktype ( "imsls_qmr" , M2(1) ,  "M2(1)" , 5 , "function" )
    end
    apifun_checktype ( "imsls_qmr" , max_it , "max_it" , 6 , "constant")
    apifun_checktype ( "imsls_qmr" , tol ,    "tol" , 7 , "constant")
    //
    // Check size
    if ( or ( typeof(A) == ["constant" "sparse"]) ) then
        apifun_checksquare ( "imsls_qmr" , A , "A" , 1 )
    end
    apifun_checkveccol ( "imsls_qmr" , b , "b" , 2 , size(b,"*") )
    apifun_checkveccol ( "imsls_qmr" , x0 , "x0" , 3 , size(x0,"*") )
    if ( or ( typeof(M1) == ["constant" "sparse"]) ) then
        if ( M1<> [] ) then
            apifun_checkdims ( "imsls_qmr" , M1 , "M1" , 4 , [n n] )
        end
    end
    if ( or ( typeof(M2) == ["constant" "sparse"]) ) then
        if ( M2<> [] ) then
            apifun_checkdims ( "imsls_qmr" , M2 , "M2" , 5 , [n n] )
        end
    end
    apifun_checkscalar ( "imsls_qmr" , max_it , "max_it" , 6 )
    apifun_checkscalar ( "imsls_qmr" , tol , "tol" , 7 )
    //
    // Check content
    apifun_checkgreq ( "imsls_qmr" , max_it , "max_it" , 6 , 1 )
	apifun_checkflint( "imsls_qmr" , max_it , "max_it" , 6)
    apifun_checkgreq ( "imsls_qmr" , tol , "tol" , 7 , number_properties("tiny") )
    //
    // Setup the Matrix-Vector product A into a couple (function,list-of-args).
    [myA_fun,myA_args] = imsls_setupMatvec(A,imsls_matvectrans);
    //
    // Setup the Left Preconditionner M1
    [myM1_fun,myM1_args] = imsls_setupPrecond(M1,imsls_precondtransfake,imsls_precondtrans);
    //
    // Setup the Right Preconditionner M2
    [myM2_fun,myM2_args] = imsls_setupPrecond(M2,imsls_precondtransfake,imsls_precondtrans);
    //
    //               Begin of computations
    //
    //
    // initialization
    i = 0;
	iter = 0;
    flag = 0;
    x = x0;

    bnrm2 = norm( b );
    if  ( bnrm2 == 0.0 ) then
        bnrm2 = 1.0;
    end

    //   r = b - A*x;
    r = b - myA_fun(x,"notransp",myA_args(1:$));
    err = norm( r ) / bnrm2;
    res = err;
    if ( err < tol ) then
        return;
    end

    // [M1,M2] = lu( M1 );

    v_tld = r;
    // y = M1 \ v_tld;
    y = myM1_fun(v_tld,"notransp",myM1_args(1:$))
    rho = norm( y );

    w_tld = r;
    //   z = M2' \ w_tld;
    z = myM2_fun(w_tld, "transp",myM2_args(1:$));
    xi = norm( z );

    gam =  1.0;
    eta = -1.0;
    theta =  0.0;

    for i = 1:max_it,                      // begin iteration

        if ( rho == 0.0 | xi == 0.0 ) then
            iter=i;
            break;
        end

        v = v_tld / rho;
        y = y / rho;

        w = w_tld / xi;
        z = z / xi;

        delta = z'*y;
        if ( delta == 0.0 ) then
            iter=i;
            break;
        end

        //    y_tld = M2 \ y;
        y_tld = myM2_fun(y,"notransp",myM2_args(1:$));
        //    z_tld = M1'\ z;
        z_tld = myM1_fun(z, "transp",myM1_args(1:$));

        if ( i > 1 ) then                       // direction vector
            p = y_tld - ( xi*delta / ep )*p;
            q = z_tld - ( rho*delta / ep )*q;
        else
            p = y_tld;
            q = z_tld;
        end

        //    p_tld = A*p;
        p_tld = myA_fun(p,"notransp",myA_args(1:$));

        ep = q'*p_tld;
        if ( ep == 0.0 ) then
            iter=i;
            break;
        end

        bet = ep / delta;
        if ( bet == 0.0 ) then
            iter=i;
            break;
        end

        v_tld = p_tld - bet*v;
        //    y =  M1 \ v_tld;
        y = myM1_fun(v_tld,"notransp",myM1_args(1:$));

        rho_1 = rho;
        rho = norm( y );
        //    w_tld = ( A'*q ) - ( bet*w );
        Atq = myA_fun(q,"transp",myA_args(1:$));
        w_tld = ( Atq ) - ( bet*w );
        //    z =  M2' \ w_tld;
        z =  myM2_fun(w_tld,"transp",myM2_args(1:$));

        xi = norm( z );

        gamma_1 = gam;
        theta_1 = theta;

        theta = rho / ( gamma_1*bet );
        gam = 1.0 / sqrt( 1.0 + (theta^2) );
        if ( gam == 0.0 ) then
            iter=i;
            break;
        end

        eta = -eta*rho_1*(gam^2) / ( bet*(gamma_1^2) );

        if ( i > 1 ) then                         // compute adjustment
            d = eta*p + (( theta_1*gam )^2)*d;
            s = eta*p_tld + (( theta_1*gam )^2)*s;
        else
            d = eta*p;
            s = eta*p_tld;
        end

        x = x + d;                               // update approximation

        r = r - s;                               // update residual
        err = norm( r ) / bnrm2;               // check convergence
        res = [res;err];

        if ( err <= tol ) then
            iter=i;
            break;
        end

        if ( i == max_it ) then
            iter=i;
        end

    end

    if ( err <= tol ) then                        // converged
        flag =  0;
    elseif ( rho == 0.0 ) then                      // breakdown
        flag = -1;
    elseif ( bet == 0.0 ) then
        flag = -2;
    elseif ( gam == 0.0 ) then
        flag = -3;
    elseif ( delta == 0.0 ) then
        flag = -4;
    elseif ( ep == 0.0 ) then
        flag = -5;
    elseif ( xi == 0.0 ) then
        flag = -6;
    else                                        // no convergence
        flag = 1;
    end

    if ( flag <> 0 ) then
        warning(msprintf(gettext("%s: Algorithm has not converged."),"imsls_qmr"))
    end
endfunction
