// Copyright (C) 2005 - INRIA - Sage Group (IRISA)
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function A = imsls_lehmer(n)
    // Returns the Lehmer matrix.
    //
    // Calling Sequence
    //  A = imsls_lehmer(n)
    //
    // Parameters
    //    n:         a 1-by-1 matrix of doubles, integer value
    //    A:         N-by-N symmetric positive definite matrix
    //
    // Description
    // Returns the symmetric positive definite N-by-N matrix with
    //                         A(i,j) = i/j for j>=i.
    //        A is totally nonnegative.  inv(A) is tridiagonal, and explicit
    //        formulas are known for its entries.
    //        N <= COND(A) <= 4*N*N.
    //
    // Bibliography
    //     M. Newman and J. Todd, The evaluation of matrix inversion programs, J. Soc. Indust. Appl. Math., 6 (1958), pp. 466-476. Solutions to problem E710 (proposed by D.H. Lehmer): The inverse of a matrix, Amer. Math. Monthly, 53 (1946), pp. 534-535.
    //
    // Examples
    // A = imsls_lehmer(5)
    //
    // Authors
    // Copyright (C) 2005 - INRIA - Sage Group (IRISA)
    // Copyright (C) 2011 - DIGITEO - Michael Baudin

    [lhs, rhs] = argn()
    apifun_checkrhs ( "imsls_lehmer" , rhs , 1 )
    //
    // Check types
    apifun_checktype ( "imsls_lehmer" , n ,  "n" , 1 , "constant")
    //
    // Check size
    apifun_checkscalar ( "imsls_lehmer" , n ,  "n" , 1 )
    //
    // Check content
    apifun_checkgreq ( "imsls_lehmer" , n ,  "n" , 1 , 1 )

    // begin of computations

    A = ones(n,1)*(1:n);
    A = A./A';
    A = tril(A) + tril(A,-1)';

endfunction
