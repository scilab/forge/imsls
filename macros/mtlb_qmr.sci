// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [x,flag,relres,iter,resvec] = mtlb_qmr(varargin )
    //     The input / output arguments of this command are the same as
    //     Matlab's qmr command.
    //     There are two differences:
	//     * reorder the input and output arguments,
	//     * change default values.

    // Load Internals lib if it's not already loaded
    // =========================================================================
    if ~ exists("imslsinternalslib") then
        path = imsls_getpath()
        imslsinternalslib = lib(fullfile(path,"macros","internals"));
    end

    [lhs, rhs] = argn()
    apifun_checkrhs ( "mtlb_qmr" , rhs , 2:7 )
    apifun_checklhs ( "mtlb_qmr" , lhs , 0:5 )

    //
    // Get input arguments
    A = varargin(1);
    b = varargin(2);
    n = size(b,"*")
    tol = apifun_argindefault(varargin , 3 , 1.e-6 )
    maxit = apifun_argindefault(varargin , 4 , min(n,20) )
    M = apifun_argindefault(varargin , 5 , [] );
    M2 = apifun_argindefault(varargin , 6 , [] );
    x0 = apifun_argindefault(varargin , 7 , zeros(n,1) );
    //
    // Check types
    apifun_checktype ( "mtlb_qmr" , A ,  "A" , 1 , ["constant" "sparse" "function" "list"])
    if ( typeof(A)=="list" ) then
        apifun_checktype ( "mtlb_qmr" , A(1) ,  "A(1)" , 1 , "function" )
    end
    apifun_checktype ( "mtlb_qmr" , b ,  "b" , 2 , ["constant" "sparse"])
    apifun_checktype ( "mtlb_qmr" , tol ,    "tol" , 3 , "constant")
    apifun_checktype ( "mtlb_qmr" , maxit , "maxit" , 4 , "constant")
    apifun_checktype ( "mtlb_qmr" , M ,  "M" , 5 , ["constant" "sparse" "function" "list"])
    if ( typeof(M)=="list" ) then
        apifun_checktype ( "mtlb_qmr" , M(1) ,  "M(1)" , 5 , "function" )
    end
    apifun_checktype ( "mtlb_qmr" , M2 , "M2" , 6 , ["constant" "sparse" "function" "list"])
    if ( typeof(M2)=="list" ) then
        apifun_checktype ( "mtlb_qmr" , M2(1) ,  "M2(1)" , 6 , "function" )
    end
    apifun_checktype ( "mtlb_qmr" , x0 , "x0" , 7 , ["constant" "sparse"])
    //
    // Check size
    if ( or ( typeof(A) == ["constant" "sparse"]) ) then
        apifun_checksquare ( "mtlb_qmr" , A , "A" , 1 )
    end
    apifun_checkveccol ( "mtlb_qmr" , b , "b" , 2 , size(b,"*") )
    apifun_checkscalar ( "mtlb_qmr" , tol , "tol" , 3 )
    apifun_checkscalar ( "mtlb_qmr" , maxit , "maxit" , 4 )
    if ( or ( typeof(M) == ["constant" "sparse"]) ) then
        if ( M<> [] ) then
            apifun_checkdims ( "mtlb_qmr" , M , "M" , 5 , [n n] )
        end
    end
    if ( or ( typeof(M2) == ["constant" "sparse"]) ) then
        if ( M2<> [] ) then
            apifun_checkdims ( "mtlb_qmr" , M2 , "M2" , 6 , [n n] )
        end
    end
    apifun_checkveccol ( "mtlb_qmr" , x0 , "x0" , 7 , size(x0,"*") )
    //
    // Check content
    apifun_checkgreq ( "mtlb_qmr" , tol , "tol" , 3 , number_properties("tiny") )
    apifun_checkgreq ( "mtlb_qmr" , maxit , "maxit" , 4 , 1 )
	apifun_checkflint( "mtlb_qmr" , maxit , "maxit" , 4)
    //
    // Extra-checks
    if ( or ( typeof(A) == ["constant" "sparse"]) ) then
        if ( bool2s(or( A ~= A')) == 1 ) then
            warning(msprintf(gettext("%s: matrix A should be symetric"),"mtlb_qmr"));
        end
    end

    [x, relres, iter, flag, resvec] = imsls_qmr(A, b, x0, M, M2, maxit, tol)
endfunction

